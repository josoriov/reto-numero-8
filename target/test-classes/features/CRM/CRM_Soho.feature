#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Title of your feature
  I want to use this template for my feature file

  @Registro
    Scenario: Registro de usuario
    Given que el usuario quiere utilizar el CRM Zoho 
    When realizo el registro exitoso 
    |Nombre |Correo 					|Contrasena	|Pais 	|
    |Juan		|josoriov@choucairtesting.com|juan1234|Colombia|	
    Then Entonces Verifico el acceso a la aplicación
    
  @CrearTarea
  Scenario: Creación de una tarea 
    Given ingreso al CRM Zoho con un usuario registrado "josoriov@choucairtesting.com" y "juan1234" 
    When realizo la creación de una Tarea 
    |Asunto_0 | Fecha de vencimiento_1|Contacto_2    | Cuenta_3 | Estado_4 | Prioridad_5 | Enviar msjemail_6 | Fecha inicio_7     | Tipo repetición_8 | Notificar_9      |Descipción_10|								
		|Reunión  | 05/08/2018            | Kris Marrier | King     | En curso | Más alto    | Si |                01/05/2018 - 08:00 | Semanal           | Ventana emergente| Tarea creada|	
    Then  verifico tarea creada exitosamente

   
    
    
