package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.CrmCrearTareaPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class CrmCrearTareaSteps {

	
	CrmCrearTareaPage crmCrearTareaPage;
	
	@Step
	public void ingresoUsuarioRegistrado(String user, String password) throws InterruptedException {
		crmCrearTareaPage.open();	
		crmCrearTareaPage.ingreso(user, password);
	}
	
	@Step
	public void crearTarea(List<String> rdatos) throws InterruptedException {
		crmCrearTareaPage.crearTarea(rdatos);
	}
	
	@Step
	public void verificarTarea(List<String> rdatos) throws InterruptedException {
		crmCrearTareaPage.verificoTareaCreada(rdatos);		
	}

}
