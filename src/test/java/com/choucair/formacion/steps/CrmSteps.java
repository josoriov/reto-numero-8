package com.choucair.formacion.steps;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import java.util.List;

import com.choucair.formacion.pageobjects.CrmPage;

import net.thucydides.core.annotations.Step;

public class CrmSteps {
	
	
	CrmPage crmPage;
	
	@Step
	public void crear_Usuario() {
		crmPage.open(); 
	}

	@Step
	public void diligenciar_formulario_datos_tabla(List<List<String>> data, int id) {
		System.out.println(data.get(id).get(0).trim());
		crmPage.nombre(data.get(id).get(0).trim());
		crmPage.email(data.get(id).get(1).trim());
		crmPage.password(data.get(id).get(2).trim());
		crmPage.SelectPais(data.get(id).get(3).trim());
		crmPage.terminos();
		crmPage.empezar();		
	}
	
	@Step
	public void verificarAcceso(String nomEmpresa, String telEmpresa) {
		try {
			Thread.sleep(10000);
		} catch (Exception e) {
		}
		
		String nombre = "Bienvenido Juan";
		crmPage.OrgName(nomEmpresa);
		crmPage.OrgName(telEmpresa);
		crmPage.clickBtnExplorar();
		String strMensaje = crmPage.lblBienvenida.getText();
		assertThat(strMensaje,  containsString(nombre));
		//colorlibMenuPage.menuFormValidation();
		//colorlibMenuPage.menuForms.click();
		//colorlibMenuPage.menuFormValidation.click();
		//String strMensaje = colorlibMenuPage.lblFormValidation.getText();
		//assertThat(strMensaje,  containsString("Popup Validation"));
		
	}
	
	
}
