package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.CrmCrearTareaSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class CrmCrearTareaDefinition {
	
	List<String> Rdatos;
	@Steps
	CrmCrearTareaSteps crmCrearTareaSteps;
	
	@Given("^ingreso al CRM Zoho con un usuario registrado \"([^\"]*)\" y \"([^\"]*)\"$")
	public void ingreso_al_CRM_Zoho_con_un_usuario_registrado_y(String user, String password) throws Throwable {
		crmCrearTareaSteps.ingresoUsuarioRegistrado(user, password);
	}

	@When("^realizo la creación de una Tarea$")
	public void realizo_la_creación_de_una_Tarea(DataTable dtDatosTarea) throws Throwable {
		List<List<String>> lDatos = dtDatosTarea.raw();

		for (int i = 1; i < lDatos.size(); i++) {
			Rdatos = lDatos.get(i);
		}
		
		crmCrearTareaSteps.crearTarea(Rdatos);
	}

	@Then("^verifico tarea creada exitosamente$")
	public void verifico_tarea_creada_exitosamente() throws Throwable {
	crmCrearTareaSteps.verificarTarea(Rdatos);
	  
	}
}
