package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.CrmSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class CrmDefinition {
	@Steps
	CrmSteps crmSteps;


	@Given("^que el usuario quiere utilizar el CRM Zoho$")
	public void que_el_usuario_quiere_utilizar_el_CRM_Zoho() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		
		crmSteps.crear_Usuario();
	}


	@When("^realizo el registro exitoso$")
	public void realizo_el_registro_exitoso(DataTable dtDatosForm) throws Throwable {
		List<List<String>> data = dtDatosForm.raw();
		for(int i=1; i <data.size();i++) {
			crmSteps.diligenciar_formulario_datos_tabla(data, i);
			try {
				Thread.sleep(5000);
			} catch (Exception e) {
				
			}
			
		}
	}

	@Then("^Entonces Verifico el acceso a la aplicación$")
	public void entonces_Verifico_el_acceso_a_la_aplicación() throws Throwable {
	 crmSteps.verificarAcceso("juan", "3213445455");
	}	
	
}
