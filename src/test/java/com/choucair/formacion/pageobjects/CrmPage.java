package com.choucair.formacion.pageobjects;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.Matchers.*;

import org.openqa.selenium.By;


@DefaultUrl("https://www.zoho.com/es-xl/crm/")
public class CrmPage extends PageObject{

	//Campo Nombre
	@FindBy(xpath="//*[@id=\'namefield\']")
	public WebElementFacade txtnombre;
	
	//Campo email
	@FindBy(id="email")
	public WebElementFacade txtemail;
	
	//Campo Contraseña
	@FindBy(name="password")
	public WebElementFacade txtpassword;
	
	//Campo pais
	@FindBy(id="country")
	public WebElementFacade optPais;
	
	//Campo Terminos
	@FindBy(id="signup-termservice")
	public WebElementFacade CheckTos;
	
	//Botón empezar
	@FindBy(id="signupbtn")
	public WebElementFacade btnComenzar;
	
	//Botón Explorar
	@FindBy(id="profileDetailBtn")
	public WebElementFacade btnExplorar;
	
	//Botón Organización
	@FindBy(id="orgName")
	public WebElementFacade txtOrgName;
	
	//Botón Organización
	@FindBy(id="orgPhone")
	public WebElementFacade txtOrgPhone;
	
	//Botón Organización
	@FindBy(id="show-uName")
	public WebElementFacade lblBienvenida;
	
	
	public void nombre(String datoPrueba) {
		txtnombre.click();
		txtnombre.clear();
		txtnombre.sendKeys(datoPrueba);
	}
	
	public void email(String datoPrueba) {
		txtemail.click();
		txtemail.clear();
		txtemail.sendKeys(datoPrueba);
	}
	
	public void password(String datoPrueba) {
		txtpassword.click();
		txtpassword.clear();
		txtpassword.sendKeys(datoPrueba);
	}
	
	public void SelectPais(String datoPrueba) {
		optPais.click();
		optPais.selectByVisibleText(datoPrueba);
	}
	
	public void terminos() {
		if ( !getDriver().findElement(By.id("tos")).isSelected())
		{
		     getDriver().findElement(By.id("tos")).click();
		}
	}

	public void empezar() {
		btnComenzar.click();
	}
	
	public void clickBtnExplorar() {
		btnExplorar.click();
	}
	
	public void OrgName(String datoPrueba) {
		txtOrgName.click();
		txtOrgName.clear();
		txtOrgName.sendKeys(datoPrueba);
	}
	
	public void OrgPhone(String datoPrueba) {
		txtOrgPhone.click();
		txtOrgPhone.clear();
		txtOrgPhone.sendKeys(datoPrueba);
	}
}
