package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.zoho.com/es-xl/crm/")
public class CrmCrearTareaPage extends PageObject {



	@FindBy(className = "signin")
	private WebElementFacade btnComenzar;

	@FindBy(id = "email-error")
	private WebElementFacade msjRegistro;

	@FindBy(xpath = "//*[contains(@href, '/accounts.zoho.com/signin?')]")
	private WebElementFacade lnkInicioSesion;

	@FindBy(id = "lid")
	private WebElementFacade txtCorreoInicio;

	@FindBy(id = "pwd")
	private WebElementFacade txtContraseniaInicio;

	@FindBy(id = "signin_submit")
	private WebElementFacade btnIniciarSesion;

	@FindBy(id = "show-uName")
	private WebElementFacade msjBienvenida;

	// TAREA

	@FindBy(id = "createIcon")
	private WebElementFacade iconoCrear;

	@FindBy(id = "submenu_Tasks")
	private WebElementFacade tarea;

	@FindBy(id = "Crm_Tasks_SUBJECT")
	private WebElementFacade asunto;

	@FindBy(id = "Crm_Tasks_DUEDATE")
	private WebElementFacade fechaVencimiento;

	@FindBy(id = "Crm_Tasks_CONTACTID")
	private WebElementFacade contacto;

	@FindBy(id = "Crm_Tasks_SEID")
	private WebElementFacade cuenta;

	@FindBy(id = "Crm_Tasks_STATUS_label")
	private WebElementFacade lblestado;
	
	@FindBy(id = "Crm_Tasks_STATUS")
	private WebElementFacade estado;

	@FindBy(xpath = "//*[@id=\"Tasks_fldRow_PRIORITY\"]/div[2]/div/span/span[1]/span")
	private WebElementFacade prioridad;

	@FindBy(id = "Crm_Tasks_SENDNOTIFICATION")
	private WebElementFacade enviarMsj;

	@FindBy(id = "Crm_Tasks_REMINDAT")
	private WebElementFacade Recordatorio;

	@FindBy(id = "Crm_Tasks_RemindAt_Start_Date1")
	private WebElementFacade fechaInicio;

	@FindBy(id = "Crm_Tasks_RemindAt_Start_Date1_TimeOption")
	private WebElementFacade horaInicio;

	@FindBy(xpath = "//*[@id=\"Tasks_fldRow_RemindAt_period\"]/div[2]/div/span/span[1]/span")
	private WebElementFacade tipoRepeticion;

	@FindBy(xpath = "//*[@id=\"Tasks_fldRow_RemindAt_Notify\"]/div[2]/label[2]/span[2]")
	private WebElementFacade notificarVentanaEmergente;

	@FindBy(xpath = "//*[@id=\"Tasks_fldRow_RemindAt_Notify\"]/div[2]/label[1]/span[2]")
	private WebElementFacade notificarCorreo;

	@FindBy(id = "Crm_Tasks_DESCRIPTION")
	private WebElementFacade descripcion;

	@FindBy(id = "saveTasksBtn_Bottom")
	private WebElementFacade btnGuardar;
	
	@FindBy(id = "headervalue_SUBJECT")
	private WebElementFacade LbltareaCreada;
	
	public void ingreso(String usuario, String contra) throws InterruptedException {
		Thread.sleep(3000);
		lnkInicioSesion.click();
		Thread.sleep(6000);
		txtCorreoInicio.sendKeys(usuario);
		txtContraseniaInicio.sendKeys(contra);
		btnIniciarSesion.click();
		Thread.sleep(8000);
	}

	
	public void crearTarea(List<String> data) throws InterruptedException {
		
		iconoCrear.click();
		tarea.click();
		System.out.println(data.get(0));
		asunto.sendKeys(data.get(0));
		Thread.sleep(1000);
		fechaVencimiento.sendKeys(data.get(1));
		Thread.sleep(1000);
		lblestado.click();
		Thread.sleep(1000);
		contacto.sendKeys(data.get(2));
		Thread.sleep(1000);
		cuenta.sendKeys(data.get(3));
		JavascriptExecutor j = (JavascriptExecutor) this.getDriver();
		j.executeScript("window.scrollBy(0," + 400 + ")", "");
		Thread.sleep(1000);
		descripcion.sendKeys(data.get(10));
		btnGuardar.click();
		Thread.sleep(6000);
	}

public void verificoTareaCreada(List<String> data) throws InterruptedException {
	
	String asuntoV = data.get(0).trim();
	String asuntoPantalla = LbltareaCreada.getTextValue();
	
	assertThat(asuntoV, containsString(asuntoPantalla));

	
}
	
}
